#include "utility.h"
#if _WIN32
int gettimeofday(struct timeval *tv, struct timezone *tz)
{
#ifdef _MSC_VER
  #define U64_LITERAL(n) n##ui64
#else
  #define U64_LITERAL(n) n##llu
#endif
	
	/* Conversion logic taken from Tor, which in turn took it
	* from Perl.  GetSystemTimeAsFileTime returns its value as
	* an unaligned (!) 64-bit value containing the number of
	* 100-nanosecond intervals since 1 January 1601 UTC. */
#define EPOCH_BIAS U64_LITERAL(116444736000000000)
#define UNITS_PER_SEC U64_LITERAL(10000000)
#define USEC_PER_SEC U64_LITERAL(1000000)
#define UNITS_PER_USEC U64_LITERAL(10)
	union {
		FILETIME ft_ft;
		uint64_t ft_64;
	} ft;
	
	if (tv == NULL)
		return -1;
	
	GetSystemTimeAsFileTime(&ft.ft_ft);
	
	if (ft.ft_64 < EPOCH_BIAS) {
		/* Time before the unix epoch. */
		return -1;
	}
	ft.ft_64 -= EPOCH_BIAS;
	tv->tv_sec = (long)(ft.ft_64 / UNITS_PER_SEC);
	tv->tv_usec = (long)((ft.ft_64 / UNITS_PER_USEC) % USEC_PER_SEC);
	return 0;
}

#endif