#ifndef __utility_h__
#define __utility_h__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#if !_WIN32
#include <unistd.h>
#include <sys/time.h>
#else
#include <winsock2.h>
struct timezone
{
	char _;
};
int gettimeofday(struct timeval *tv, struct timezone *tz);
#endif

#endif